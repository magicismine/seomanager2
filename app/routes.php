<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	//return Redirect::to('/home');
	return View::make('root');
});

Route::get('/home', function(){
});

Route::post('/users/auth-token', 'UserController@postAuthToken');
Route::get('/users/token', 'UserController@getToken');
Route::put('/users/logout', 'UserController@putLogout');
Route::resource('users', 'UserController');

Route::put('/todolist/{id}/update-user-todo', 'TodolistController@putUpdateUserTodo');
Route::resource('todolist', 'TodolistController');
?>