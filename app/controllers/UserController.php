<?php

class UserController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function postAuthToken(){
		$temp = Input::all();
		$O = User::where('username', $temp['uname'])->where('password', $temp['upass'])->get();
		$filter = array_filter($O->toArray());
		if(!empty($filter)){	
			$token = bin2hex(openssl_random_pseudo_bytes(16));
			$res = array('token' => $token);
			$user = User::find($O->toArray()[0]['id']);
			$user->token = $token;
			$user->save();
			Session::put('user.token', $token);
			Session::put('user.id', $user->id);
		}
		else{
			$res = array('token' => "0");
		}
		return json_encode($res);
	}

	public function getToken(){
		return Session::get('token', '0');
	}

	public function putLogout(){
		$token = getallheaders()['Auth-Token'];
		if(Session::get('user.token', '0') == $token){
			Session::forget('token');
			return 1;
		}
		return 0;
	}

	public function index(){
		$token = getallheaders()['Auth-Token'];
		if(Session::get('user.token', '0') == $token){
			$O = User::find(Session::get('user.id', '0'));
			$filter = array_filter($O->toArray());
			if(!empty($filter)){
				$res = $O->toArray();
				return json_encode($res);
			}
		}
		return 0;
	}

	public function create(){
		echo "create";
	}

	public function store(){
		print_r(Input::all());
		$O = new User;
		$O->username = Input::get('username');
		$O->name = Input::get('name');
		$O->email 	 = Input::get('email');
		$O->password = Input::get('password');
		$O->webAddress = Input::get('webAddress');
		$O->save();
		return $O->id;
	}

	public function show($id){
		$token = getallheaders()['Auth-Token'];
		if(Session::get('user.token', '0') == $token){
			$O = User::find($id);
			$filter = array_filter($O->toArray());
			if(!empty($filter)){
				$res = $O->toArray();
				return json_encode($res);
			}
		}
		return 0;
	}

	public function edit($id){
		echo "edit : ". $id;
	}

	public function update($id){
	    echo "update";
	}
}
