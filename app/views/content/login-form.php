		<div class="jumbotron" ng-controller="loginController">
			<h3> Welcome to SEO Manager </h3>
			<form class="form-horizontal" role="form">
			  	<div class="form-group">
				    <div class="col-xs-2"> <p> Username </p></div>
				    <div class="col-xs-2">
			      		<input type="text" class="form-control" placeholder="Enter Username" ng-model="username">
			    	</div>
			  </div>
			  <div class="form-group">
				    <div class="col-xs-2"> <p> Password </p></div>
				    <div class="col-xs-2">
			      		<input type="password" class="form-control" placeholder="Password" ng-model="userpassword">
			    	</div>
			  </div>
			  <div class="form-group">
			    <div class="col-sm-offset-2 col-xs-1">
			      	<button type="submit" class="btn btn-default" ng-click="userLogin()">Sign in</button>
			    </div>
			    <div class="col-xs-1">
			      	<a class="btn btn-info" href="#/register">Register</a>
			    </div>
			  </div>
			</form>
			<div class="alert alert-warning" ng-show="isError"> Error login! Username and Password didn't match. </div>
		</div>