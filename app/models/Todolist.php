<?php

class Todolist extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'todo_lists';
	public $timestamps = false;
}

?>