-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 13, 2014 at 07:41 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `seomanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `back_links`
--

CREATE TABLE IF NOT EXISTS `back_links` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `user_id` int(3) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `url` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1_back_links` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `back_links`
--

INSERT INTO `back_links` (`id`, `user_id`, `name`, `url`) VALUES
(1, 1, 'Facebook', 'www.facebook.com'),
(2, 1, 'Twitter', 'www.twitter.com'),
(3, 1, 'Kaskus', 'www.kaskus.co.id'),
(4, 2, 'Facebook', 'www.facebook.com'),
(5, 2, 'Instagram', 'www.instagram.co.id');

-- --------------------------------------------------------

--
-- Table structure for table `todo_lists`
--

CREATE TABLE IF NOT EXISTS `todo_lists` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `url` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `todo_lists`
--

INSERT INTO `todo_lists` (`id`, `name`, `type`, `url`) VALUES
(1, 'Google Analytics', 'onsite', NULL),
(2, 'Robots.txt', 'onsite', NULL),
(3, 'Facebook Content', 'offsite', 'www.facebook.com'),
(4, 'Twitter Content', 'offsite', 'www.twitter.com'),
(5, 'Pinterest Content', 'offsite', 'www.pinterest.com'),
(6, 'Instagram Content', 'offsite', 'www.instagram.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` varchar(8) DEFAULT NULL,
  `webAddress` varchar(40) DEFAULT NULL,
  `token` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `webAddress`, `token`) VALUES
(1, 'Singgih Mardianto', 'singgihms', 'singgihmardianto@gmail.com', '123123', 'www.singgihmardianto.co.id', '6e19167c61a629ef16cc63d65ad2455a'),
(2, 'test test', 'test123', 'test@smart-it.co.id', '123123', 'www.test.smart-it.co.id', '53dfe644588dc137ee6d1035d9050202'),
(3, 'hello hello', 'hello', 'hello@smart-it.co.id', '123123', 'www.hello.com', 'd1441eaf4c5b45d26f626162db50ea4b'),
(4, 'somebody body', 'somebody', 'somebody@gmail.com', '123123', 'www.somebody.com', '355e125b36d07686771b2ddc5df8b838'),
(5, 'Someone with Somebod', 'someone', 'someone@gmail.com', '123123', 'www.someone.com', '7e35d611aad6cc2b9e8017238afc0f6f');

-- --------------------------------------------------------

--
-- Table structure for table `users_todolists`
--

CREATE TABLE IF NOT EXISTS `users_todolists` (
  `user_id` int(3) NOT NULL DEFAULT '0',
  `todo_list_id` int(3) NOT NULL DEFAULT '0',
  `last_update` date DEFAULT NULL,
  PRIMARY KEY (`user_id`,`todo_list_id`),
  KEY `FK2_todolists_id` (`todo_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_todolists`
--

INSERT INTO `users_todolists` (`user_id`, `todo_list_id`, `last_update`) VALUES
(1, 2, '2014-02-13'),
(1, 3, '2014-02-13'),
(1, 4, '2014-02-13'),
(3, 1, '2014-02-13'),
(3, 2, '2014-02-13'),
(3, 3, '2014-02-13'),
(3, 4, '2014-02-13'),
(3, 5, '2014-02-13'),
(3, 6, '2014-02-13'),
(4, 3, '2014-02-13'),
(4, 4, '2014-02-13'),
(5, 1, '2014-02-13'),
(5, 2, '2014-02-13'),
(5, 3, '2014-02-13');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `back_links`
--
ALTER TABLE `back_links`
  ADD CONSTRAINT `FK1_back_links` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users_todolists`
--
ALTER TABLE `users_todolists`
  ADD CONSTRAINT `FK1_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FK2_todolists_id` FOREIGN KEY (`todo_list_id`) REFERENCES `todo_lists` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
