var myServices = angular.module('UserServices', ['ngResource']);
 
myServices.factory('User', ['$http', '$window', '$q', 'Helper', function($http, $window, $q, Helper){
		var User = {};

		User.login = function(_method, _url, _params){
			return Helper.http(_method, _url, _params);
		}
		
		User.register = function(_method, _url, _params){
			return Helper.http(_method, _url, _params);
		}
		return User;
	}
]);

