var myServices = angular.module('HelperServices', ['ngResource']);
 
myServices.factory('Helper', ['$http', '$window', '$q', function($http, $window, $q){
		var help = {};


		help.http = function (_header, _method, _url, _object, _function, _id, _params) {
			$http.defaults.headers.common['Auth-Token'] = $window.sessionStorage.token;
			if(_object != "")
				_url += "/" + _object;
			if(_id != "")
				_url += "/" + _id;
			if(_function != "")
				_url += "/" + _function;
			
			if(_method == "GET"){
				var temp = {
					headers : _header,
					url : _url,
					method: _method
				}
			}
			else if(_method == "POST" || _method == "PUT"){
				var temp = {
					headers : _header,
					url : _url,
					method: _method,
					data: _params
				}
			}
			// console.log(_url);
			var deferred = $q.defer();
			$http(temp).success(function(data){
				if(data != "null")
					deferred.resolve(data);
				else{
					console.log('error : ',data);
					deferred.reject(data);
				}
			}).error(function(data){
				deferred.reject(data);
				console.log('error : ',data);
			})
			return deferred.promise;
		}
		
		return help;
	}
]);

