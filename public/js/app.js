var myApp = angular.module('App', [
  'ng',
  'ngRoute',
  'mainModule',
  'HelperServices',
  'UserServices'
]);


myApp.config(['$routeProvider', '$locationProvider',function($routeProvider, $locationProvider) {
    $routeProvider
      	.when('/login',
      	{
        	templateUrl: "/SEOManager2/app/views/content/login-form.php"
      	})
      	.when('/register',
      	{
        	templateUrl: "/SEOManager2/app/views/content/register-form.php"
      	})
        .when('/home',
        {
          templateUrl: "/SEOManager2/app/views/content/home.php"
        })
      	.otherwise({
        	redirectTo: "/login"
      	})

    // $locationProvider.html5Mode(true);
}]);