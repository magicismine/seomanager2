
var index = angular.module('mainModule',[]);

index.controller('rootController',['$scope','$http','Helper','$q', '$location',
	function($scope, $http, Helper, $q, $location){
		$scope.name = "this is root controller";
	}
]);

index.controller('loginController', ['$scope','$http','Helper','$q', '$location', '$window',
	function($scope, $http, Helper, $q, $location, $window){
		$scope.name = "this is login controller";
		$scope.isError = false;
		$scope.userLogin = function(){
			$scope.param = { 'uname' : $scope.username, 'upass' : $scope.userpassword };
			Helper
			.http("POST", "http://localhost/SEOManager2/public/users/auth-token", $scope.param)
			.then(function(data){
				$window.sessionStorage.token = data.token;
				if(data.token != "0")
					$location.path('/home');
				else
					$scope.isError = true;
			}, function(data){
				$scope.isError = true;
				console.log(data);
			});
		}
	}
])

index.controller('registerController', ['$scope','$http','Helper','$q', '$location', '$timeout', 
	function($scope, $http, Helper, $q, $location, $timeout){
		$scope.message = "";
		$scope.isError = false;
		$scope.userRegister = function(){
			if($scope.passRegistry == $scope.cpassRegistry && typeof $scope.mailRegistry != "undefined"){
				$scope.param = { 
					'username' : $scope.unameRegistry, 
					'name' : $scope.nameRegistry, 
					'email' : $scope.mailRegistry, 
					'password' : $scope.passRegistry, 
					'webAddress' : $scope.webAddressRegistry
				};
				Helper
				.http("POST", "http://localhost/SEOManager2/public/users", $scope.param)
				.then(function(data){
					$scope.message = "Congratulation data has been added, you'll be redirect to login page.";
					$scope.isError = true;

					$timeout(function() {
						$location.path("/login");
					}, 2000);
				}, function(data){
					$scope.isError = true;
					console.log(data);
				});
			}
			else{
				$scope.message = "Error, register doesn't finished.";
				$scope.isError = true;
			}
		}
	}
])

index.controller('homeController', ['$scope','$http','Helper','$q', '$location', '$window',
	function($scope, $http, Helper, $q, $location, $window){
		$scope.token = null;
		$scope.user = null;
		$scope.todolists = {};

		if(typeof $window.sessionStorage.token == "undefined")
			$location.path('/login');

		Helper
		.http("GET", "http://localhost/SEOManager2/public/users", "")
		.then(function(data){
			console.log(data);
			$scope.user = data;
			Helper
			.http("GET", ("http://localhost/SEOManager2/public/todolist/" + $scope.user.id), "")
			.then(function(data){
				if(data != null){
					$scope.todolists = data;
				}
				else
					console.log("Error, Todolist doesn't have any record. s")
			}, function(data){
				console.log(data);
			});
		}, function(data){
			console.log(data);
		});

		$scope.logout = function(){
			Helper
			.http("PUT", "http://localhost/SEOManager2/public/users/logout", "")
			.then(function(data){
				console.log(data);
				if(data == 1){
					console.log('Logout.');
					delete $window.sessionStorage.token;
					$location.path('/login');
				}
			}, function(data){
				console.log(data);
			});
		}

		$scope.updateUserTodo = function(t){
			// console.log(t);
			Helper
			.http("PUT", ("http://localhost/SEOManager2/public/todolist/" + $scope.user.id + "/update-user-todo"), t)
			.then(function(data){
				console.log(data);
			}, function(data){
				console.log(data);
			});
		}
	}
])